//
//  MapActionsViewController.swift
//  Map
//
//  Created by Eyal Cohen on 30/07/2020.
//  Copyright © 2020 Grubhub, Inc. All rights reserved.
//

import UIKit

class MapActionsViewController : UITableViewController {
    let actions: [MapAction] = [.addElement, .centerUserLocation, .zoomUserLocation, .printRadius]

    weak var delegate: MapActionsViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Map Actions"
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actions.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let deqCell = tableView.dequeueReusableCell(withIdentifier: "cell")
        let cell = deqCell ?? UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = actions[indexPath.row].rawValue
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let action = actions[indexPath.row]
        delegate?.mapActionsViewController(self, didSelect: action)
    }
}

enum MapAction: String {
    case addElement = "Add Element"
    case centerUserLocation = "Center user location"
    case zoomUserLocation = "Zoom user location"
    case printRadius = "Print radius"
}

protocol MapActionsViewControllerDelegate : class {
    func mapActionsViewController(_ vc: MapActionsViewController, didSelect action: MapAction)
}
