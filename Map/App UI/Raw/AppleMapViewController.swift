//
//  AppleMapViewController.swift
//  Map
//
//  Created by Eyal Cohen on 26/07/2020.
//  Copyright © 2020 Grubhub, Inc. All rights reserved.
//

import UIKit
import MapKit

class AppleMapViewController: UIViewController {

    @IBOutlet var mapView: MKMapView!

    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.delegate = self

        addElement()

//        let ul = mapView.userLocation.coordinate
        let cr = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 32.0694213, longitude: 34.7860188), latitudinalMeters: 1000, longitudinalMeters: 1000)
        mapView.setRegion(cr, animated: true)

        mapView.showsUserLocation = true

        mapView.pointOfInterestFilter = MKPointOfInterestFilter(excluding: [.hospital, .school])

        drawRoute()
    }

    func addElement() {
        //32.0694213, 34.7860188
        let work = MKPointAnnotation()
        work.title = "Work"
        work.coordinate = CLLocationCoordinate2D(latitude: 32.0694213, longitude: 34.7860188)
        mapView.addAnnotation(work)
    }

    func drawRoute() {
        let sourceLocation = CLLocationCoordinate2D(latitude: 32.0694213, longitude: 34.7860188)
        let destinationLocation = CLLocationCoordinate2D(latitude: 32.077461, longitude: 34.777809)

        // 3.
        let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)

        // 4.
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)

        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile

        // Calculate the direction
        let directions = MKDirections(request: directionRequest)

        directions.calculate { (response, error) -> Void in

            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                return
            }

            guard let route = response.routes.first else {
                return
            }

            DispatchQueue.main.async {
                self.drawPath(route)
            }
        }
    }

    func drawPath(_ route: MKRoute) {
        self.mapView.addOverlay(route.polyline, level: .aboveRoads)

        let rect = route.polyline.boundingMapRect.insetBy(dx: -1000, dy: -1000)
        self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
    }
}

extension AppleMapViewController : MKMapViewDelegate {

//    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
//        print(">>> regionWillChangeAnimated")
//    }

//    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
//        print("apple: regionDidChangeAnimated")
//    }

    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        print("apple: mapViewDidChangeVisibleRegion \(Date())")
    }

    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        print("apple: calloutAccessoryControlTapped")
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        print("apple: didSelect")
    }

    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        print("apple: didDeselect")
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 4.0

        return renderer
    }
}

