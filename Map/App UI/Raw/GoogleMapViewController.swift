//
//  GoogleMapViewController.swift
//  Map
//
//  Created by Eyal Cohen on 26/07/2020.
//  Copyright © 2020 Grubhub, Inc. All rights reserved.
//

import UIKit
import GoogleMaps

class GoogleMapViewController: UIViewController {

    var mapView: GMSMapView!
    let apiKey = "AIzaSyA7rjmudEH1CJmw61vhokPm-vgv5X5qD90"

    override func viewDidLoad() {
        super.viewDidLoad()

        GMSServices.provideAPIKey(apiKey)

        // Do any additional setup after loading the view.
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        let camera = GMSCameraPosition.camera(withLatitude: 32.0694213, longitude: 34.7860188, zoom: 16.0)
        mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        mapView.delegate = self
        self.view.addSubview(mapView)



        mapView.isMyLocationEnabled = true

        applyStyle()

        drawRoute()
    }

    func addElement() {
        //32.0694213, 34.7860188
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: 32.0694213, longitude: 34.7860188)
        //        marker.title = "Sydney"
        //        marker.snippet = "Australia"
        marker.map = mapView
    }

    func applyStyle() {
        do {
          // Set the map style by passing the URL of the local file.
          if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
            mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
          } else {
            NSLog("Unable to find style.json")
          }
        } catch {
          NSLog("One or more of the map styles failed to load. \(error)")
        }
    }

    func drawRoute() {
        let source = CLLocationCoordinate2D(latitude: 32.0694213, longitude: 34.7860188)
        let destination = CLLocationCoordinate2D(latitude: 32.077461, longitude: 34.777809)

        let session = URLSession.shared

        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=false&mode=driving&key=\(apiKey)")!

        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in

            guard error == nil else {
                print(error!.localizedDescription)
                return
            }

            guard let jsonResponse = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] else {
                return
            }

            guard let routes = jsonResponse["routes"] as? [Any] else {
                return
            }

            guard let route = routes.first as? [String: Any] else {
                return
            }

            guard let overview_polyline = route["overview_polyline"] as? [String: Any] else {
                return
            }

            guard let polyLineString = overview_polyline["points"] as? String else {
                return
            }
            DispatchQueue.main.async {
                self.drawPath(from: polyLineString)
            }
        })
        task.resume()
    }

    func drawPath(from polyLineString: String) {
        let path = GMSPath(fromEncodedPath: polyLineString)!
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = .systemBlue
        polyline.strokeWidth = 5
        polyline.map = self.mapView


        let bounds = GMSCoordinateBounds(path: path)
        let padding: CGFloat = 20
        if let camPos = mapView.camera(for: bounds, insets: .init(top: padding, left: padding, bottom: padding, right: padding)) {
            mapView.camera = camPos
        }
    }
}

extension GoogleMapViewController : GMSMapViewDelegate {

    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        print("google: didChange position \(Date())")
        print("google: mapView.camera.zoom: \(mapView.camera.zoom)")
    }

    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("google: didTap marker")
        return false
    }
}
