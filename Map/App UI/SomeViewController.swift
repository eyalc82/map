//
//  SomeViewController.swift
//  Map
//
//  Created by Eyal Cohen on 29/07/2020.
//  Copyright © 2020 Grubhub, Inc. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class SomeViewController : UIViewController {
    let workCoord = CLLocationCoordinate2D(latitude: 32.0694213, longitude: 34.7860188)

    var searchingLocation: CLLocationCoordinate2D!

    var mapController: MapViewControlling!
    
    var needsUserLocationFocus = false
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var researchContainer: UIStackView!


    override func viewDidLoad() {
        super.viewDidLoad()

        searchingLocation = workCoord
        addMap(type: .apple)
    }

    @IBAction func searchThisAreaButtonTapped(_ sender: UIButton) {
        // perform search in a new location
    }

    @IBAction func goToUserLocationButtonTapped(_ sender: UIButton) {
        centerOnUserLocation(animated: true)
        showReasearchContainer(false, animated: true)
    }

    func showReasearchContainer(_ show: Bool, animated: Bool) {
        let change = { [unowned self] in
            self.researchContainer.alpha = show ? 1 : 0
        }
        if animated {
            UIView.animate(withDuration: 0.45, animations: change)
        } else {
            change()
        }
    }
}


// MARK: - Adding the map

extension SomeViewController {
    enum MapType {
        case google, apple
    }

    private func addMap(type: MapType) {
        if let mapController = mapController {
            // remove prev map
            mapController.view.removeFromSuperview()
        }
        switch type {
        case .apple:
            mapController = AppleMapController()
        case .google:
            mapController = GoogleMapController()
        }

        mapController.delegate = self

        let mapView = mapController.view
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.insertSubview(mapView, at: 0)
        mapView.frame = contentView.bounds

        // reset
        resetMap()
    }

    private func resetMap() {
        zoomOnSearchingLocation(animated: false)
        showReasearchContainer(false, animated: false)
        showUserLocation(true)
    }

    @IBAction func switchMap(_ sender: UIBarButtonItem) {
        if mapController is AppleMapController {
            addMap(type: .google)
        } else {
            addMap(type: .apple)
        }
    }
}


// MARK: - Actions on map

extension SomeViewController {
    @IBAction func actionsButtonTapped(_ sender: UIBarButtonItem) {
        let actionsVC = MapActionsViewController(style: .plain)
        actionsVC.delegate = self
        let nav = UINavigationController(rootViewController: actionsVC)
        present(nav, animated: true, completion: nil)
    }

    func zoomOnSearchingLocation(animated: Bool) {
        mapController.zoom(on: searchingLocation, zoomLevel: 15, animated: animated)
    }


    func zoomOnUserLocation(animated: Bool) {
        guard let ul = mapController.userLocation else {
            return
        }
        mapController.zoom(on: ul, zoomLevel: 15, animated: animated)
    }

    func centerOnUserLocation(animated: Bool) {
        guard let ul = mapController.userLocation else {
            return
        }
        mapController.center(on: ul, animated: animated)
    }

    func showUserLocation(_ show: Bool) {
        mapController.showsUserLocation = show
//        if show, let _ = mapController.userLocation {
//            focusOnUserLocation(animated: true)
//        } else {
//            needsUserLocationFocus = true
//        }
    }

    func addRandomRestaurantOnMap() {
        let lat = Double.random(in: 0.001...0.005)
        let long = Double.random(in: 0.001...0.005)
        let coord = CLLocationCoordinate2D(latitude: workCoord.latitude + lat, longitude: workCoord.longitude + long)
        let element = MapElement(coordinate: coord, title: "Restaurant")
        mapController.addElement(element)

        mapController.center(on: coord, animated: true)
    }
}


// MARK: - MapActionsViewControllerDelegate
extension SomeViewController : MapActionsViewControllerDelegate {
    func mapActionsViewController(_ vc: MapActionsViewController, didSelect action: MapAction) {
        switch action {
        case .addElement:
            addRandomRestaurantOnMap()
        case .centerUserLocation:
            centerOnUserLocation(animated: true)
        case .zoomUserLocation:
            zoomOnUserLocation(animated: true)
        case .printRadius:
            print("currentRadius: \(mapController.currentRadius)")
        }
        vc.dismiss(animated: true, completion: nil)
    }
}


// MARK: - MapViewDelegate

extension SomeViewController : MapControllerDelegate {

    func mapControllerDidFinishLoadingMap(_ mapController: MapViewControlling) {
        // empty for now
    }

    func mapControllerWillBeginChangeVisibleRegion(_ mapController: MapViewControlling) {
        // empty for now
    }

    func mapControllerDidChangeVisibleRegion(_ mapController: MapViewControlling) {
        // empty for now
    }

    func mapControllerDidFinishChangeVisibleRegion(_ mapController: MapViewControlling) {
        // find distance between the seaching location and the center of the map
        let l1 = CLLocation(latitude: searchingLocation.latitude, longitude: searchingLocation.longitude)
        let l2 = CLLocation(latitude: mapController.centerCoordinate.latitude, longitude: mapController.centerCoordinate.longitude)
        let distanceInMeters = l1.distance(from: l2)

        // show the research container if the distance is bigger than 1 metter
        if distanceInMeters > 1 {
            showReasearchContainer(true, animated: true)
        }
    }

    func mapController(_ mapController: MapViewControlling, didUpdate userLocation: CLLocationCoordinate2D?) {
//        guard needsUserLocationFocus, let userLocation = userLocation else {
//            return
//        }
//        needsUserLocationFocus = false
//        mapController.focus(on: userLocation, animated: true)
    }
}
