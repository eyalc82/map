//
//  MapControllerDelegate.swift
//  Map
//
//  Created by Eyal Cohen on 29/07/2020.
//  Copyright © 2020 Grubhub, Inc. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

protocol MapControllerDelegate : class {

    func mapController(_ mapController: MapViewControlling, didUpdate userLocation: CLLocationCoordinate2D?)

    func mapControllerDidFinishLoadingMap(_ mapController: MapViewControlling)

    func mapControllerWillBeginChangeVisibleRegion(_ mapController: MapViewControlling)
    func mapControllerDidChangeVisibleRegion(_ mapController: MapViewControlling)
    func mapControllerDidFinishChangeVisibleRegion(_ mapController: MapViewControlling)
}
