//
//  MapElement.swift
//  Map
//
//  Created by Eyal Cohen on 29/07/2020.
//  Copyright © 2020 Grubhub, Inc. All rights reserved.
//

import Foundation
import CoreLocation

struct MapElement {
    var coordinate: CLLocationCoordinate2D
    var title: String
}
