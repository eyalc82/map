//
//  MapViewControlling.swift
//  Map
//
//  Created by Eyal Cohen on 29/07/2020.
//  Copyright © 2020 Grubhub, Inc. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

protocol MapViewControlling {
    var view: UIView { get }

    var delegate: MapControllerDelegate? { get set }

    var userLocation: CLLocationCoordinate2D? { get }

    var centerCoordinate: CLLocationCoordinate2D { get }

    var currentRadius: CLLocationDistance { get }

    var showsUserLocation: Bool { get set }

    func addElement(_ element: MapElement)

    func zoom(on location: CLLocationCoordinate2D, zoomLevel: UInt, animated: Bool)

    func center(on location: CLLocationCoordinate2D, animated: Bool)
}

