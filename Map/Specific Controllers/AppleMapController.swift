//
//  AppleMapController.swift
//  Map
//
//  Created by Eyal Cohen on 29/07/2020.
//  Copyright © 2020 Grubhub, Inc. All rights reserved.
//

import Foundation
import MapKit

class AppleMapController : NSObject, MapViewControlling {

    private var mapView: MKMapView

    weak var delegate: MapControllerDelegate?

    var view: UIView {
        return mapView
    }

    var userLocation: CLLocationCoordinate2D? {
        return mapView.userLocation.location?.coordinate
    }

    var centerCoordinate: CLLocationCoordinate2D {
        return mapView.centerCoordinate
    }

    var currentRadius: CLLocationDistance {
        return mapView.currentRadius
    }

    var showsUserLocation: Bool {
        set {
            mapView.showsUserLocation = newValue
        }
        get {
            return mapView.showsUserLocation
        }
    }

    override init() {
        mapView = MKMapView()

        super.init()

        mapView.delegate = self
    }

    func addElement(_ element: MapElement) {
        let annotation = MKPointAnnotation()
        annotation.title = element.title
        annotation.coordinate = element.coordinate
        mapView.addAnnotation(annotation)
    }

    func zoom(on location: CLLocationCoordinate2D, zoomLevel: UInt, animated: Bool) {
        mapView.zoom(toCenterCoordinate: location, zoomLevel: zoomLevel, animated: animated)
    }

    func center(on location: CLLocationCoordinate2D, animated: Bool) {
        mapView.setCenter(location, animated: animated)
    }
}


extension AppleMapController : MKMapViewDelegate {

    // user location changed
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        delegate?.mapController(self, didUpdate: userLocation.coordinate)
    }

    // map region changed
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        delegate?.mapControllerWillBeginChangeVisibleRegion(self)
    }

    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        delegate?.mapControllerDidChangeVisibleRegion(self)
    }

    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        delegate?.mapControllerDidFinishChangeVisibleRegion(self)
    }

    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        delegate?.mapControllerDidFinishLoadingMap(self)
    }
}


// MARK: - Radius

extension MKMapView {
    var topCenterCoordinate: CLLocationCoordinate2D {
        return convert(CGPoint(x: frame.size.width / 2.0, y: 0), toCoordinateFrom: self)
    }

    var currentRadius: CLLocationDistance {
        let centerLocation = CLLocation(latitude: centerCoordinate.latitude, longitude: centerCoordinate.longitude)
        let topCenterLocation = CLLocation(latitude: topCenterCoordinate.latitude, longitude: topCenterCoordinate.longitude)
        return centerLocation.distance(from: topCenterLocation)
    }
}


// MARK: - Zoom by level

extension MKMapView {
    var MERCATOR_OFFSET : Double {
        return 268435456.0
    }

    var MERCATOR_RADIUS : Double  {
        return 85445659.44705395
    }

    private func longitudeToPixelSpaceX(longitude: Double) -> Double {
        return round(MERCATOR_OFFSET + MERCATOR_RADIUS * longitude * Double.pi / 180.0)
    }

    private func latitudeToPixelSpaceY(latitude: Double) -> Double {
        return round(MERCATOR_OFFSET - MERCATOR_RADIUS * log((1 + sin(latitude * Double.pi / 180.0)) / (1 - sin(latitude * Double.pi / 180.0))) / 2.0)
    }

    private  func pixelSpaceXToLongitude(pixelX: Double) -> Double {
        return ((round(pixelX) - MERCATOR_OFFSET) / MERCATOR_RADIUS) * 180.0 / Double.pi;
    }

    private func pixelSpaceYToLatitude(pixelY: Double) -> Double {
        return (Double.pi / 2.0 - 2.0 * atan(exp((round(pixelY) - MERCATOR_OFFSET) / MERCATOR_RADIUS))) * 180.0 / Double.pi;
    }

    private func coordinateSpan(withMapView mapView: MKMapView, centerCoordinate: CLLocationCoordinate2D, zoomLevel: UInt) -> MKCoordinateSpan {
        let centerPixelX = longitudeToPixelSpaceX(longitude: centerCoordinate.longitude)
        let centerPixelY = latitudeToPixelSpaceY(latitude: centerCoordinate.latitude)

        let zoomExponent = Double(20 - zoomLevel)
        let zoomScale = pow(2.0, zoomExponent)

        let mapSizeInPixels = mapView.bounds.size
        let scaledMapWidth =  Double(mapSizeInPixels.width) * zoomScale
        let scaledMapHeight = Double(mapSizeInPixels.height) * zoomScale

        let topLeftPixelX = centerPixelX - (scaledMapWidth / 2);
        let topLeftPixelY = centerPixelY - (scaledMapHeight / 2);

        //    // find delta between left and right longitudes
        let minLng = pixelSpaceXToLongitude(pixelX: topLeftPixelX)
        let maxLng = pixelSpaceXToLongitude(pixelX: topLeftPixelX + scaledMapWidth)
        let longitudeDelta = maxLng - minLng;

        let minLat = pixelSpaceYToLatitude(pixelY: topLeftPixelY)
        let maxLat = pixelSpaceYToLatitude(pixelY: topLeftPixelY + scaledMapHeight)
        let latitudeDelta = -1 * (maxLat - minLat);

        return MKCoordinateSpan(latitudeDelta: latitudeDelta, longitudeDelta: longitudeDelta)
    }

    func zoom(toCenterCoordinate centerCoordinate:CLLocationCoordinate2D ,zoomLevel: UInt, animated: Bool) {
        let zoomLevel = min(zoomLevel, 20)
        let span = coordinateSpan(withMapView: self, centerCoordinate: centerCoordinate, zoomLevel: zoomLevel)
        let region = MKCoordinateRegion(center: centerCoordinate, span: span)
        setRegion(region, animated: animated)
    }
}
