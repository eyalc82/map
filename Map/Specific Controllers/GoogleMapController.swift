//
//  GoogleMapController.swift
//  Map
//
//  Created by Eyal Cohen on 29/07/2020.
//  Copyright © 2020 Grubhub, Inc. All rights reserved.
//

import Foundation
import GoogleMaps

class GoogleMapController : NSObject, MapViewControlling {

    let apiKey = "AIzaSyA7rjmudEH1CJmw61vhokPm-vgv5X5qD90"
    
    private var mapView: GMSMapView

    weak var delegate: MapControllerDelegate?
    
    var view: UIView {
        return mapView
    }

    var userLocation: CLLocationCoordinate2D? {
        return mapView.myLocation?.coordinate
    }

    var centerCoordinate: CLLocationCoordinate2D {
        return mapView.camera.target
    }

    var currentRadius: CLLocationDistance {
        return mapView.currentRadius
    }

    var showsUserLocation: Bool {
        set {
            mapView.isMyLocationEnabled = newValue
        }
        get {
            return mapView.isMyLocationEnabled
        }
    }

    override init() {
        GMSServices.provideAPIKey(apiKey)
        mapView = GMSMapView()

        super.init()

        mapView.delegate = self

        mapView.addObserver(self, forKeyPath: "myLocation", options: .new, context: nil)
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let update = change, let myLocation = update[.newKey] as? CLLocation else {
            return
        }
        delegate?.mapController(self, didUpdate: myLocation.coordinate)
    }

    deinit {

    }

    func addElement(_ element: MapElement) {
        let marker = GMSMarker()
        marker.position = element.coordinate
        marker.snippet = element.title
        marker.map = mapView
    }


    func zoom(on location: CLLocationCoordinate2D, zoomLevel: UInt, animated: Bool) {
        let update = GMSCameraUpdate.setTarget(location, zoom: Float(zoomLevel))
        if animated {
            mapView.animate(with: update)
        } else {
            mapView.moveCamera(update)
        }
    }

    func center(on location: CLLocationCoordinate2D, animated: Bool) {
        let update = GMSCameraUpdate.setTarget(location)
        if animated {
            mapView.animate(with: update)
        } else {
            mapView.moveCamera(update)
        }
    }
}


extension GoogleMapController : GMSMapViewDelegate {
    // map region changed
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        delegate?.mapControllerWillBeginChangeVisibleRegion(self)
    }

    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        delegate?.mapControllerDidChangeVisibleRegion(self)
    }

    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        delegate?.mapControllerDidFinishChangeVisibleRegion(self)
    }

    func mapViewSnapshotReady(_ mapView: GMSMapView) {
        delegate?.mapControllerDidFinishLoadingMap(self)
    }
}


// MARK: - Radius

extension GMSMapView {
    var topCenterCoordinate: CLLocationCoordinate2D {
        let center = convert(CGPoint(x: frame.size.width / 2.0, y: 0), to: self)
        return projection.coordinate(for: center)
    }

    var currentRadius: CLLocationDistance {
        let centerCoordinate = projection.coordinate(for: center)
        let centerLocation = CLLocation(latitude: centerCoordinate.latitude, longitude: centerCoordinate.longitude)
        let topCenterLocation = CLLocation(latitude: topCenterCoordinate.latitude, longitude: topCenterCoordinate.longitude)
        return centerLocation.distance(from: topCenterLocation)
    }
}
